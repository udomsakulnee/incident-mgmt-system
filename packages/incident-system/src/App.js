import React, { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";

const Login = React.lazy(() => import("./pages/Login/Login"));
const Home = React.lazy(() => import("./pages/Home/Home"));

const loading = () => <div>Loading...</div>;
const App = () => {
  return (
    <Router>
      <Suspense fallback={loading}>
        <Switch>
          <Route path="/home"  component={Home} />
          <Route path="/" component={Login} />
        </Switch>
      </Suspense>
    </Router>
  );
};
export default App;
