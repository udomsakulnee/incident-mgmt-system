import React, { useState } from "react";
import { Layout, Menu, Form, Input, Button, Table } from "antd";
import axios from "axios";

const { Header, Content } = Layout;

const Home = () => {
  const [showPage, setShowPage] = useState(0);
  const [data, setData] = useState([]);

  const columns = [
    {
      title: "TicketId",
      dataIndex: "TicketId",
    },
    {
      title: "LocationMachine",
      dataIndex: "LocationMachine",
    },
    {
      title: "Subject",
      dataIndex: "Subject",
    },
    {
      title: "SLA",
      dataIndex: "SLA",
    },
    {
      title: "ProblemType",
      dataIndex: "ProblemType"
    },
    {
      title: "ProblemDesc",
      dataIndex: "ProblemDesc"
    },
    {
      title: "ProblemDate",
      dataIndex: "ProblemDate"
    }
  ];

  const onSave = async (data) => {
    console.log("on finish", data);

    await axios.post("http://localhost:1337/incidents", data);
    alert("save success");
  };

  const onSearch = async (data) => {
    console.log("on finish", data);

    let response;
    if (typeof data.TicketId !== 'undefined' && data.TicketId !== '') {
      response = await axios.get(
        `http://localhost:1337/incidents?TicketId_in=${data.TicketId}&LocationMachine_in=${data.LocationMachine}&ProblemDate_gt=${data.ProblemDateFrom}&ProblemDate_lt=${data.ProblemDateTo}`
      );
    }
    else {
      response = await axios.get(
        'http://localhost:1337/incidents'
      );
    }
    if (response.data.length > 0) {
      setData(response.data);
    }
  };

  const renderSelect = () => (
    <div>
      <Form name="nest-messages" onFinish={onSearch}>
        <Form.Item name="TicketId" label="TicketId">
          <Input />
        </Form.Item>
        <Form.Item name="LocationMachine" label="LocationMachine">
          <Input />
        </Form.Item>
        <Form.Item name="ProblemDateFrom" label="ProblemDateFrom">
          <Input />
        </Form.Item>
        <Form.Item name="ProblemDateTo" label="ProblemDateTo">
          <Input />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Search
          </Button>
        </Form.Item>
      </Form>

      <div>
        <Table columns={columns} dataSource={data} size="middle" />
      </div>
    </div>
  );

  const renderInsert = () => (
    <Form name="nest-messages" onFinish={onSave}>
      <Form.Item name="TicketId" label="TicketId">
        <Input />
      </Form.Item>
      <Form.Item name="Subject" label="Subject">
        <Input />
      </Form.Item>
      <Form.Item name="SLA" label="SLA">
        <Input />
      </Form.Item>
      <Form.Item name="ProblemType" label="ProblemType">
        <Input />
      </Form.Item>
      <Form.Item name="ProblemDesc" label="ProblemDesc">
        <Input />
      </Form.Item>
      <Form.Item name="ProblemDate" label="ProblemDate">
        <Input />
      </Form.Item>
      <Form.Item name="SolveUser" label="SolveUser">
        <Input />
      </Form.Item>
      <Form.Item name="SolveRole" label="SolveRole">
        <Input />
      </Form.Item>
      <Form.Item name="SolveDate" label="SolveDate">
        <Input />
      </Form.Item>
      <Form.Item name="SolveDesc" label="SolveDesc">
        <Input />
      </Form.Item>
      <Form.Item name="Cause" label="Cause">
        <Input />
      </Form.Item>
      <Form.Item name="CurrentStatus" label="CurrentStatus">
        <Input />
      </Form.Item>
      <Form.Item name="Delay" label="Delay">
        <Input />
      </Form.Item>
      <Form.Item name="CloseTicketUser" label="CloseTicketUser">
        <Input />
      </Form.Item>
      <Form.Item name="CloseTicketRole" label="CloseTicketRole">
        <Input />
      </Form.Item>
      <Form.Item name="CloseTicketDate" label="CloseTicketDate">
        <Input />
      </Form.Item>
      <Form.Item name="PicSlot1" label="PicSlot1">
        <Input />
      </Form.Item>
      <Form.Item name="PicSlot2" label="PicSlot2">
        <Input />
      </Form.Item>
      <Form.Item name="PicSlot3" label="PicSlot3">
        <Input />
      </Form.Item>
      <Form.Item name="PicSlot4" label="PicSlot4">
        <Input />
      </Form.Item>
      <Form.Item name="Transaction_id" label="Transaction_id">
        <Input />
      </Form.Item>
      <Form.Item name="LocationMachine" label="LocationMachine">
        <Input />
      </Form.Item>
      <Form.Item name="MachineCode" label="MachineCode">
        <Input />
      </Form.Item>
      <Form.Item name="ManchineName" label="ManchineName">
        <Input />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );

  return (
    <Layout>
      <Header className="header">
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1" onClick={() => setShowPage(0)}>
            select
          </Menu.Item>
          <Menu.Item key="2" onClick={() => setShowPage(1)}>
            insert
          </Menu.Item>
        </Menu>
      </Header>
      <Layout>
        <Layout style={{ padding: "0 24px 24px" }}>
          <Content
            className="site-layout-background"
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            {showPage === 0 ? renderSelect() : renderInsert()}
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
};
export default Home;
